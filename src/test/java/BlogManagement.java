import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

public class BlogManagement {
    public static void main(String[] args) throws Exception {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        //Login blank submit test
        driver.get("https://staging.studylinkclasses.com/login");
        driver.findElement(By.id("kt_login_signin_submit")).click();
        boolean isEmailRequiredErrorMessageVisible = driver.findElement(By.xpath("//strong[normalize-space()='The email field is required.']")).isDisplayed();
        boolean isPasswordRequiredErrorMessageVisible = driver.findElement(By.xpath("//strong[normalize-space()='The password field is required.']")).isDisplayed();
        if (!(isEmailRequiredErrorMessageVisible && isPasswordRequiredErrorMessageVisible)) {
            throw new Exception("Email or password required message not visible");
        }

        //Login credentials does not match test
        driver.findElement(By.name("email")).sendKeys("randomusername");
        driver.findElement(By.name("password")).sendKeys("12345678");
        driver.findElement(By.id("kt_login_signin_submit")).click();
        boolean isCredentialsDoesNotMatchMessageVisible = driver.findElement(By.xpath("//strong[normalize-space()='These credentials do not match our records.']")).isDisplayed();
        if (!isCredentialsDoesNotMatchMessageVisible) {
            throw new Exception("Credentials Does Not Match error message not visible");
        }

        //Correct Login Test
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys("author34@studylinkclasses.com");
        driver.findElement(By.name("password")).sendKeys("abcd1234");
        driver.findElement(By.id("kt_login_signin_submit")).click();

        String adminPanelTitle = driver.findElement(By.cssSelector("h3.kt-header__title")).getText();
        if (!adminPanelTitle.equals("Study Link -Admin Panel")) {
            throw new Exception("Login Failed or admin panel title not visble!");
        }

        String dashboardURL = driver.getCurrentUrl();
        if (!dashboardURL.equals("https://staging.studylinkclasses.com/control-panel/dashboard")) {
            throw new Exception("Login Failed or Redirected to wrong URL after login!");

        }

        //Locating elements with RelativeLocator
        WebElement siteLogo = driver.findElement(By.id("kt_header_brand"));
        WebElement dashboardLink = driver.findElement(RelativeLocator.with(By.tagName("a")).below(siteLogo));

        if(!dashboardLink.isDisplayed()){
            throw new Exception("Dashboard Link is not visible!");
        }

        WebElement blogsLink = driver.findElement(RelativeLocator.with(By.tagName("a")).below(dashboardLink));
        if(!blogsLink.isDisplayed()){
            throw new Exception("Blogs Link is not visible!");
        }

        blogsLink.click();
        boolean isManageBlogsTitleVisible = driver.findElement(By.xpath("//h3[normalize-space()='Manage Blogs']")).isDisplayed();

        if(!isManageBlogsTitleVisible){
            throw new Exception("Redirected to wrong page after clicking on blogs or title not visible");
        }

        //Validate URL

        driver.findElement(By.xpath("//a[normalize-space()='Create Blog']")).click();

        //Validate URL

        WebElement title = driver.findElement(By.name("title"));
        WebElement excerpt = driver.findElement(By.name("excerpt"));
        WebElement tagsSearchInput = driver.findElement(By.xpath("//input[@placeholder='Search']"));

        WebElement tagsAddButton = driver.findElement(By.xpath("//div[contains(@class,'dual-listbox__buttons')]//button[normalize-space()='Add']"));
        WebElement imageInput = driver.findElement(By.name("image"));
        WebElement publishedAt = driver.findElement(By.name("published_at"));
        WebElement seoDescription = driver.findElement(By.name("seo_description"));
        WebElement seoKeywords = driver.findElement(By.name("seo_keywords"));
        WebElement seoOGTitle = driver.findElement(By.name("seo_og_title"));
        WebElement seoOGDescription = driver.findElement(By.name("seo_og_description"));

        WebElement submitButton = driver.findElement(By.xpath("//button[normalize-space()='Submit']"));
        if(!title.isDisplayed()){
            throw new Exception("Title input not visible");
        }
        if(!excerpt.isDisplayed()){
            throw new Exception("Excerpt input not visible");
        }
        if(!tagsSearchInput.isDisplayed()){
            throw new Exception("Tag Search input not visible");
        }
        if(!tagsAddButton.isDisplayed()){
            throw new Exception("Tag Add button not visible");
        }
        if(!publishedAt.isDisplayed()){
            throw new Exception("Published At input not visible");
        }
        if(!seoDescription.isDisplayed()){
            throw new Exception("SEO Description input not visible");
        }
        if(!seoKeywords.isDisplayed()){
            throw new Exception("SEO Keywords input not visible");
        }
        if(!seoOGTitle.isDisplayed()){
            throw new Exception("SEO OG Title input not visible");
        }
        if(!seoOGDescription.isDisplayed()){
            throw new Exception("SEO OG Description not visible");
        }

        HashMap<String,Object> createdBlogData = new HashMap<>();
        Faker faker = new Faker();

        String fakeTitle = faker.lorem().sentence();
        title.sendKeys(fakeTitle);
        createdBlogData.put("title",fakeTitle);

        String fakeExcerpt = faker.lorem().sentence(10);
        excerpt.sendKeys(fakeExcerpt);
        createdBlogData.put("excerpt",fakeExcerpt);



        String fakeBody = faker.lorem().paragraph(20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blog-body_ifr")));
        driver.switchTo().frame("blog-body_ifr");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")));
        driver.findElement(By.id("tinymce")).sendKeys(fakeBody);
        driver.switchTo().parentFrame();
        createdBlogData.put("body",fakeBody);


        String[] tags = {"laravel","coding","php"};
        for(int i=0;i<tags.length;i++){
            tagsSearchInput.sendKeys(tags[i]);
            driver.findElement(By.xpath
                    ("//ul[@class='dual-listbox__available']/li[normalize-space()='"+tags[i]+"']")).click();
            tagsAddButton.click();
            tagsSearchInput.clear();
            String selectedTagName = driver.findElement(By.xpath("//ul[@class='dual-listbox__selected']/li["+(i+1)+"]")).getText();
            if(!selectedTagName.equals(tags[i])){
                throw new Exception("Select tag component not working");
            }
        }
        createdBlogData.put("tags",tags);

        File f = new File("src/test/resources/images/blogImage.png");
        String imagePath = f.getAbsolutePath();
        imageInput.sendKeys(imagePath);

        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String formattedDate = localDateTime.format(formatter);
        publishedAt.sendKeys(formattedDate);
        createdBlogData.put("publishedAt",formattedDate);


        String fakeDescription = faker.lorem().sentence(11);
        seoDescription.sendKeys(fakeDescription);
        createdBlogData.put("description",fakeDescription);


        String fakeKeyWords = faker.lorem().sentence(10).replaceAll("\\s",",");
        seoKeywords.sendKeys(fakeKeyWords);
        createdBlogData.put("keyWords",fakeKeyWords);


        String fakeSEOTitle = faker.lorem().sentence(7);
        seoOGTitle.sendKeys(fakeSEOTitle);
        createdBlogData.put("seoTitle",fakeSEOTitle);


        String fakeSEODescription = faker.lorem().sentence(14);
        seoOGDescription.sendKeys(fakeSEODescription);
        createdBlogData.put("seoDescription",fakeSEODescription);


        submitButton.click();

        boolean isBlogCreatedSuccessfully = driver.findElement(By.xpath("//div[contains(text(),'Blog was created Successfully!')]")).isDisplayed();
        if(!isBlogCreatedSuccessfully){
            throw new Exception("There is some problem in blog creation");
        }

        //Validating created blog
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)createdBlogData.get("title"));
        Thread.sleep(5000);
        driver.findElement(By.xpath("//td[normalize-space()='"+((String)createdBlogData.get("title"))+"']/parent::tr/td/div/a/i[contains(@class,'la la-eye')]")).click();

        validateCreatedBlog(driver,createdBlogData);

        driver.navigate().back();

        driver.findElement(By.xpath("//div[@class='kt-header__topbar-wrapper']")).click();
        driver.findElement(By.xpath("//a[normalize-space()='Sign Out']")).click();

        driver.get("https://staging.studylinkclasses.com/login");

        login(driver,"admin34@studylinkclasses.com","abcd1234");

//        driver.findElement(By.xpath("//a[@href='https://staging.studylinkclasses.com/control-panel/blogs']")).click();

        //Visiting blogs page with Actions Class
        WebElement sidebarBlogsLink = driver.findElement(By.xpath("//a[@href='https://staging.studylinkclasses.com/control-panel/blogs']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(sidebarBlogsLink);
        actions.perform();
        sidebarBlogsLink.click();

        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)createdBlogData.get("title"));

        Thread.sleep(5000);

        String approvedByStatus = driver.findElement(By.xpath("//table[@id='blogs_datatable']/tbody/tr/td[normalize-space()='"+createdBlogData.get("title")+"']/parent::tr/td[7]")).getText();
        if(!approvedByStatus.equals("Pending")){
            throw new Exception("");
        }

        String approvedAtStatus = driver.findElement(By.xpath("//table[@id='blogs_datatable']/tbody/tr/td[normalize-space()='"+createdBlogData.get("title")+"']/parent::tr/td[8]")).getText();
        if(!approvedAtStatus.equals("Pending")){
            throw new Exception("");
        }

        driver.findElement(By.xpath("//table[@id='blogs_datatable']/tbody/tr/td[normalize-space()='"+createdBlogData.get("title")+"']/parent::tr/td/div/a/i[contains(@class,'la la-eye')]")).click();
        validateCreatedBlog(driver,createdBlogData);

        driver.get("https://staging.studylinkclasses.com/blogs");

        driver.findElement(By.xpath("//input[@placeholder='Search Blogs']")).sendKeys((String)createdBlogData.get("title"), Keys.ENTER);

        String extractedFetchedBlogsCount = driver.findElement(By.xpath("//div[contains(@class,'row')]/div[contains(text(),'We found')]/strong")).getText();

        if(!extractedFetchedBlogsCount.equals("0")){
            List<WebElement> fetchedBlogCardTitles = driver.findElements(By.xpath("//div[@class='articles_grid_style']//div[@class='articles_grid_caption']/h4/a"));
            for (int i=0;i<fetchedBlogCardTitles.size();i++){
                String extractedBlogTitle = fetchedBlogCardTitles.get(i).getText().trim();
                if(extractedBlogTitle.equalsIgnoreCase((String)createdBlogData.get("Title"))){
                    throw new Exception("");
                }
            }
        }

        driver.get("https://staging.studylinkclasses.com/control-panel/blogs");
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)createdBlogData.get("title"));

        Thread.sleep(5000);

        driver.findElement(By.xpath("//table[@id='blogs_datatable']/tbody/tr/td[normalize-space()='"+createdBlogData.get("title")+"']/parent::tr/td[9]")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='approveModal']//button[normalize-space()='Close']")));
        WebElement closeButton = driver.findElement(By.xpath("//div[@id='approveModal']//button[normalize-space()='Close']"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='approveModal']//button[normalize-space()='Approve']")));
        WebElement approveButton = driver.findElement(By.xpath("//div[@id='approveModal']//button[normalize-space()='Approve']"));

        if(!(closeButton.isDisplayed())){
            throw new Exception("Close Modal button not visible");
        }

        if(!(approveButton.isDisplayed())){
            throw new Exception("Approve Modal button not visible");
        }

        approveButton.click();

        WebElement successfullyApprovedAlert = driver.findElement(By.xpath("//div[@class='alert-text'][contains(text(),'Blog was approved Successfully')]"));
        if(!successfullyApprovedAlert.isDisplayed()){
            throw new Exception("");
        }

        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)createdBlogData.get("title"));
        Thread.sleep(5000);

        String disapproveButton = driver.findElement(By.xpath("//table[@id='blogs_datatable']/tbody/tr/td[normalize-space()='"+createdBlogData.get("title")+"']/parent::tr/td[9]")).getText().trim();

        if(!disapproveButton.equals("Disapprove")){
            throw new Exception("");
        }

        driver.quit();






    }
    public static void login(WebDriver driver, String username,String password) throws Exception {
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(username);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("kt_login_signin_submit")).click();

        String adminPanelTitle = driver.findElement(By.cssSelector("h3.kt-header__title")).getText();
        if(!adminPanelTitle.equals("Study Link -Admin Panel")){
            throw new Exception("Login Failed or admin panel title not visble!");
        }

        String dashboardURL = driver.getCurrentUrl();
        if(!dashboardURL.equals("https://staging.studylinkclasses.com/control-panel/dashboard"))
        {
            throw new Exception("Login Failed or Redirected to wrong URL after login!");
        }
    }
    public static void validateCreatedBlog(WebDriver driver, HashMap<String, Object> createdBlogData) throws Exception{
        String extractedBlogTitle = driver.findElement(By.cssSelector("h1.breadcrumb-title")).getText();
        if(!((String)createdBlogData.get("title")).equalsIgnoreCase(extractedBlogTitle)){
            throw new Exception("Blog title does not match on preview from author login");
        }

        String extractedBreadCrumbText = driver.findElement(By.cssSelector("li.breadcrumb-item.active")).getText();
        if(!((String)createdBlogData.get("title")).equals(extractedBreadCrumbText)){
            throw  new Exception("");
        }

        WebElement blogImage = driver.findElement(By.cssSelector("div.article_featured_image>img"));
        if(!blogImage.isDisplayed()){
            throw new Exception("");
        }

        String extractedBlogPostTitle = driver.findElement(By.cssSelector("h2.post-title")).getText();
        if(!((String)createdBlogData.get("title")).equalsIgnoreCase(extractedBlogPostTitle)){
            throw new Exception("");
        }

        String extractedBlogBody = driver.findElement(By.xpath("//div[@class='article_body_wrap']/p[1]")).getText();
        if(!extractedBlogBody.equals((String)createdBlogData.get("body"))){
            throw new Exception("");
        }

        List<WebElement> extractedBlogtags = driver.findElements(By.xpath("//div[@class='article_bottom_info']/div[@class='post-tags']/ul[@class='list']/li"));

        String[] ogBlogTags = (String[])createdBlogData.get("tags");
        if(ogBlogTags.length != extractedBlogtags.size()){
            throw new Exception("");
        }

        for(int i=0;i<extractedBlogtags.size();i++){
            if(!extractedBlogtags.get(i).getText().equals(ogBlogTags[i])){
                throw new Exception("");
            }

        }    }
}
